import json
from difflib import get_close_matches


data = json.load(open("data.json"))

def buscar(clave):
    clave = clave.lower()    
    if clave in data:
        return(data[clave])

    elif len(get_close_matches(clave, data.keys())[0]) > 0:
        yn = input("La palabra que busca es " + get_close_matches(clave, data.keys())[0] + "?. Si esa es la palabra que busca, presione S, sino, presione N:")
        if yn == "s" or input == "S":
            return(data[get_close_matches(clave, data.keys())[0]])
        else:
            return("No se encontro la palabra. Revise la ortografia.")
    else:
        return("No se encontro la palabra. Revise la ortografia.")

clave = input("Ingrese la palabra que desea buscar: ")

print(buscar(clave))